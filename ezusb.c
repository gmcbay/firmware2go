# include  <stdio.h>
# include  <errno.h>
# include  <assert.h>
# include  <limits.h>
# include  <stdlib.h>
# include  <string.h>
# include  "lusb0_usb.h"
# include "ezusb.h"

int verbose;

static int fx2_is_external (unsigned short addr, size_t len)
{
    /* 1st 8KB for data/code, 0x0000-0x1fff */
    if (addr <= 0x1fff)
	return ((addr + len) > 0x2000);

    /* and 512 for data, 0xe000-0xe1ff */
    else if (addr >= 0xe000 && addr <= 0xe1ff)
	return ((addr + len) > 0xe200);

    /* otherwise, it's certainly external */
    else
	return 1;
}

static int dataNum = 0;

static inline int ctrl_msg (
    FILE			*outfp,
    unsigned char			requestType,
    unsigned char			request,
    unsigned short			value,
    unsigned short			index,
    unsigned char			*data,
    size_t					length
) {
	int i;

	fprintf(outfp, "\tdata%d := [%d]byte{", dataNum, length);

	for (i = 0; i < length; i++) {
		fprintf(outfp, "0x%x,", data[i]);
	}

	fprintf(outfp, "\n\t}\n\n");

	fprintf(outfp, "\tif result := C.usb_control_msg(devHandle,\n0x%x,\n0x%x,\n0x%x,\n%d,\n(*C.char)(unsafe.Pointer(&data%d[0])),\n%d,\nusbTimeout); result < 0 {\n",
		requestType, request, value, index, dataNum, length);

	fprintf(outfp, "\t\terr = fmt.Errorf(\"dmd: Failure in initCypressFx2 (%d): %%v\", result)\n\t\treturn\n\t}\n\n", dataNum);

	dataNum++;

	return 1;
}

#define RW_INTERNAL	0xA0		/* hardware implements this one */
#define RW_EEPROM	0xA2
#define RW_MEMORY	0xA3
#define GET_EEPROM_SIZE	0xA5

static int ezusb_write (
    FILE *outfp,
    char				*label,
    unsigned char			opcode,
    unsigned short			addr,
    const unsigned char			*data,
    size_t				len
) {
    int					status;

    status = ctrl_msg (outfp,
	USB_DIR_OUT | USB_TYPE_VENDOR | USB_RECIP_DEVICE, opcode,
	addr, 0,
	(unsigned char *) data, len);
    if (status != len) {
	if (status < 0)
	    printf("%s: %s\n", label, strerror(errno));
	else
	    printf("%s ==> %d\n", label, status);
    }
    return status;
}

/*
 * Modifies the CPUCS register to stop or reset the CPU.
 * Returns false on error.
 */
static int ezusb_cpucs (
    FILE *outfp,
    unsigned short	addr,
    int			doRun
) {
    int			status;
    unsigned char	data = doRun ? 0 : 1;

    if (doRun) {
    	fprintf(outfp, "\t// start usb device cpu...\n");
    } else {
    	fprintf(outfp, "\t// stop usb device cpu...\n");
    }

    if (verbose)
	printf("%s\n", data ? "stop CPU" : "reset CPU");
    status = ctrl_msg (outfp,
	USB_DIR_OUT | USB_TYPE_VENDOR | USB_RECIP_DEVICE,
	RW_INTERNAL,
	addr, 0,
	&data, 1);
    if (status != 1) {
	char *mesg = "can't modify CPUCS";
	if (status < 0)
	    printf("%s: %s\n", mesg, strerror(errno));
	else
	    printf("%s\n", mesg);
	return 0;
    } else
	return 1;
}

int parse_ihex (
    FILE	*image,
    void	*context,
    int		(*is_external)(unsigned short addr, size_t len),
    int 	(*poke) (void *context, unsigned short addr, int external,
		      const unsigned char *data, size_t len)
)
{
    unsigned char	data [1023];
    unsigned short	data_addr = 0;
    size_t		data_len = 0;
    int			rc;
    int			first_line = 1;
    int			external = 0;

    for (;;) {
	char 		buf [512], *cp;
	char		tmp, type;
	size_t		len;
	unsigned	idx, off;

	cp = fgets(buf, sizeof buf, image);
	if (cp == 0) {
	    printf("EOF without EOF record!\n");
	    break;
	}

	/* EXTENSION: "# comment-till-end-of-line", for copyrights etc */
	if (buf[0] == '#')
	    continue;

	if (buf[0] != ':') {
	    printf("not an ihex record: %s", buf);
	    return -2;
	}

	/* ignore any newline */
	cp = strchr (buf, '\n');
	if (cp)
	    *cp = 0;

	if (verbose >= 3)
	    printf("** LINE: %s\n", buf);

	/* Read the length field (up to 16 bytes) */
	tmp = buf[3];
	buf[3] = 0;
	len = strtoul(buf+1, 0, 16);
	buf[3] = tmp;

	/* Read the target offset (address up to 64KB) */
	tmp = buf[7];
	buf[7] = 0;
	off = strtoul(buf+3, 0, 16);
	buf[7] = tmp;

	/* Initialize data_addr */
	if (first_line) {
	    data_addr = off;
	    first_line = 0;
	}

	/* Read the record type */
	tmp = buf[9];
	buf[9] = 0;
	type = strtoul(buf+7, 0, 16);
	buf[9] = tmp;

	/* If this is an EOF record, then make it so. */
	if (type == 1) {
	    if (verbose >= 2)
		printf("EOF on hexfile\n");
	    break;
	}

	if (type != 0) {
	    printf("unsupported record type: %u\n", type);
	    return -3;
	}

	if ((len * 2) + 11 > strlen(buf)) {
	    printf("record too short?\n");
	    return -4;
	}

	if (data_len != 0
		    && (off != (data_addr + data_len)
			// || !merge
			|| (data_len + len) > sizeof data)) {
	    if (is_external)
		external = is_external (data_addr, data_len);
	    rc = poke (context, data_addr, external, data, data_len);
	    if (rc < 0)
		return -1;
	    data_addr = off;
	    data_len = 0;
	}

	/* append to saved data, flush later */
	for (idx = 0, cp = buf+9 ;  idx < len ;  idx += 1, cp += 2) {
	    tmp = cp[2];
	    cp[2] = 0;
	    data [data_len + idx] = strtoul(cp, 0, 16);
	    cp[2] = tmp;
	}
	data_len += len;
    }


    /* flush any data remaining */
    if (data_len != 0) {
	if (is_external)
	    external = is_external (data_addr, data_len);
	rc = poke (context, data_addr, external, data, data_len);
	if (rc < 0)
	    return -1;
    }
    return 0;
}

typedef enum {
    _undef = 0,
    internal_only,		/* hardware first-stage loader */
    skip_internal,		/* first phase, second-stage loader */
    skip_external		/* second phase, second-stage loader */
} ram_mode;

struct ram_poke_context {
    FILE *outfp;
    ram_mode	mode;
    unsigned	total, count;
};

# define RETRY_LIMIT 5

static int ram_poke (
    void		*context,
    unsigned short	addr,
    int			external,
    const unsigned char	*data,
    size_t		len
) {
    struct ram_poke_context	*ctx = context;
    int			rc;
    unsigned		retry = 0;

    switch (ctx->mode) {
    case internal_only:		/* CPU should be stopped */
	if (external) {
	    printf("can't write %d bytes external memory at 0x%04x\n",
		len, addr);
	    return -EINVAL;
	}
	break;
    case skip_internal:		/* CPU must be running */
	if (!external) {
	    if (verbose >= 2) {
		printf("SKIP on-chip RAM, %d bytes at 0x%04x\n",
		    len, addr);
	    }
	    return 0;
	}
	break;
    case skip_external:		/* CPU should be stopped */
	if (external) {
	    if (verbose >= 2) {
		printf("SKIP external RAM, %d bytes at 0x%04x\n",
		    len, addr);
	    }
	    return 0;
	}
	break;
    default:
	printf("bug\n");
	return -EDOM;
    }

    ctx->total += len;
    ctx->count++;

    /* Retry this till we get a real error. Control messages are not
     * NAKed (just dropped) so time out means is a real problem.
     */
    while ((rc = ezusb_write (ctx->outfp,
		    external ? "write external" : "write on-chip",
		    external ? RW_MEMORY : RW_INTERNAL,
		    addr, data, len)) < 0
		&& retry < RETRY_LIMIT) {
	  retry += 1;
    }
    return (rc < 0) ? rc : 0;
}

int ezusb_load_ram (const char *path, const char *outputPath)
{
	int stage = 0;
    FILE			*image;
    unsigned short		cpucs_addr;
    int				(*is_external)(unsigned short off, size_t len);
    struct ram_poke_context	ctx;
    int				status;

    FILE *outfp = fopen(outputPath, "w");
    image = fopen (path, "r");

    fprintf(outfp, "package dmd\n\n/*\n#include \"lusb0_usb.h\"\n*/\nimport \"C\"\nimport (\n\"fmt\"\n\"unsafe\"\n)\nfunc initCypressFx2(devHandle *C.struct_dev_handle) (err error) {\n");

    if (image == 0) {
	printf("%s: unable to open for input.\n", path);
	return -2;
    } else if (verbose)
	printf("open RAM hexfile image %s\n", path);

    /* EZ-USB original/FX and FX2 devices differ, apart from the 8051 core */
	cpucs_addr = 0xe600;
	is_external = fx2_is_external;

    /* use only first stage loader? */
    if (!stage) {
	ctx.mode = internal_only;

	/* don't let CPU run while we overwrite its code/data */
	if (!ezusb_cpucs (outfp, cpucs_addr, 0))
	    return -1;

    /* 2nd stage, first part? loader was already downloaded */
    } else {
	ctx.mode = skip_internal;

	/* let CPU run; overwrite the 2nd stage loader later */
	if (verbose)
	    printf("2nd stage:  write external memory\n");
    }
    
    /* scan the image, first (maybe only) time */
    ctx.outfp = outfp;
    ctx.total = ctx.count = 0;
    status = parse_ihex (image, &ctx, is_external, ram_poke);
    if (status < 0) {
	printf("unable to download %s\n", path);
	return status;
    }

    /* second part of 2nd stage: rescan */
    if (stage) {
	ctx.mode = skip_external;

	/* don't let CPU run while we overwrite the 1st stage loader */
	if (!ezusb_cpucs (outfp, cpucs_addr, 0))
	    return -1;

	/* at least write the interrupt vectors (at 0x0000) for reset! */
	rewind (image);
	if (verbose)
	    printf("2nd stage:  write on-chip memory\n");
	status = parse_ihex (image, &ctx, is_external, ram_poke);
	if (status < 0) {
	    printf("unable to completely download %s\n", path);
	    return status;
	}
    }

    if (verbose)
	printf("... WROTE: %d bytes, %d segments, avg %d\n",
	    ctx.total, ctx.count, ctx.total / ctx.count);
	
    /* now reset the CPU so it runs what we just downloaded */
    if (!ezusb_cpucs (outfp, cpucs_addr, 1))
	return -1;

	fprintf(outfp, "\nreturn\n}\n");

    return 0;
}