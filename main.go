package main

/*
// Used to take a firmware .bin and .hex image and pre-process it into a Go byte arrays
// to be embedded into the dmd library code
// Go version by George McBay (george.mcbay@gmail.com)
// based on https://github.com/grinner/Polonator/tree/master/polonator/illum/libD4000
// and fxload-libusb

#include <stdio.h>
#include <stdlib.h>
#include  <stdlib.h>
#include  <stdio.h>
#include  <getopt.h>
#include  <string.h>
#include  <fcntl.h>
#include  <unistd.h>
#include "lusb0_usb.h"
#include  "ezusb.h"
#include <errno.h>
#include <stdarg.h>

#define MAX_TRANSFER_SIZE 63488

//Swap B1B2 to B2B1
void BreakSwapBytes(unsigned char file_buffer_swapped[][MAX_TRANSFER_SIZE],
					unsigned char *file_buffer,
					int *file_size, int NumberOfTransfers) {
	int BitField[256];
	long fsize = *file_size;
	int counter = 0;
	int i = 0;
	long q = 0;

	//Create the Lookup table for swaps
	for(i = 0; i < 256;i++) {
		int k = i;

		BitField[i] = 0;

		int b;

		for(b=0;b<8;b++)
		{
			BitField[i]<<=1;
			BitField[i] |= k & 1;
			k >>= 1;
		}
	}

	//Swap the old data and put into buffer
	for(i=0; i < NumberOfTransfers && counter < fsize; i++) {
		for(q=0; q < MAX_TRANSFER_SIZE && counter < fsize; q++) {
			file_buffer_swapped[i][q] = BitField[file_buffer[counter]];
			counter++;
		}
	}

	//Must zero out the last transfer to make it a nice even transfer (multiple of 512)
	for(; q < MAX_TRANSFER_SIZE;q++) {
		file_buffer_swapped[i-1][q] = 0x00;
	}
}

void convert(const char *FileName, const char *outputFilename) {
	FILE *fp;
	unsigned char *binData;
	int length = 0;

	fp = fopen(FileName, "rb"); //open file for reading

	if (fp == NULL) {
		printf("Cannot open firmware .bin file.\n");
		return;
	}

	fseek(fp, 0, SEEK_END);
	length = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	binData = malloc(length);

	if(!binData) {
		fclose(fp);
		printf("Error while trying to allocate .bin file buffer\n");
		return;
	}

	fread(binData,1,length,fp);
	fclose(fp);

	int NumberOfTransfers = 0;

	if(length % MAX_TRANSFER_SIZE == 0) {
		NumberOfTransfers = length / MAX_TRANSFER_SIZE;
	} else {
		NumberOfTransfers = (length / MAX_TRANSFER_SIZE) + 1;
	}

	unsigned char (*file_buffer_swapped)[MAX_TRANSFER_SIZE] = malloc(NumberOfTransfers * MAX_TRANSFER_SIZE * sizeof(unsigned char));

	BreakSwapBytes(file_buffer_swapped, binData, &length, NumberOfTransfers); //Create a new array with the bytes swapped, and the transfers broken up

	FILE *out = fopen(outputFilename, "w+");

	fprintf(out, "\npackage dmd\n\n// #include \"lusb0_usb.h\"\nimport \"C\"\n\nimport (\n\t\"fmt\"\n\t\"unsafe\"\n)\n\nfunc initFpgaFirmware(devHandle  *C.struct_dev_handle) (err error) {");

	int i;

	for(i = 0; i < NumberOfTransfers; i++) {
		fprintf(out, "\ndata%d := [%d]byte{", i, MAX_TRANSFER_SIZE);

		int j;

		for (j = 0; j < MAX_TRANSFER_SIZE; j++) {
			fprintf(out, "0x%x", file_buffer_swapped[i][j]);

			if (j != MAX_TRANSFER_SIZE - 1) {
				fprintf(out, ",");
			}
		}

		fprintf(out, "}\n");

		fprintf(out, "\nif res := C.usb_bulk_write(devHandle, usbEndPointFpgaProg|C.USB_ENDPOINT_OUT,(*C.char)(unsafe.Pointer(&data%d[0])),%d,usbTimeout); res <= 0 {\nerr = fmt.Errorf(\"dmd: Unable to program FPGA firmware [write error]: %%v\\n\", res)\nreturn\n}\n", i, MAX_TRANSFER_SIZE);
	}

	fprintf(out, "\nreturn\n}\n");

	fclose(out);

	free(file_buffer_swapped);
	free(binData);
}
*/
import "C"

func main() {
	C.convert(C.CString("D4000_usb.bin"), C.CString("../dmd/fpga_firmware.go"))
	C.ezusb_load_ram(C.CString("D4000_usb.hex"), C.CString("../dmd/cypress_fx2.go"))
}
